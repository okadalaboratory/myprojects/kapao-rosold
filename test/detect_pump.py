#!/usr/bin/env python
import sys
from pathlib import Path
FILE = Path(__file__).absolute()
sys.path.append(FILE.parents[1].as_posix())  # add kapao/ to path
sys.path.append('../kapao/')

import argparse
import os.path as osp
from utils.torch_utils import select_device, time_sync
from utils.general import check_img_size
from utils.datasets import LoadWebcam
from models.experimental import attempt_load
import torch
import cv2
import yaml
from tqdm import tqdm
import imageio
from val import run_nms, post_process_batch
import numpy as np
import gdown
import csv

import rospy
from sensor_msgs.msg import Image 
from cv_bridge import CvBridge
import cv2

from torchvision import transforms
transform_fn = transforms.Compose([
    transforms.ToTensor(),
#    transforms.Normalize([.485, .456, .406], [.229, .224, .225]),
])
def process_image(msg):
    try:
        bridge = CvBridge()
        orig = bridge.imgmsg_to_cv2(msg, "bgr8")
        orig = cv2.resize(orig, dsize=(1024,768))
        #img = cv2.cvtColor(orig, cv2.COLOR_RGB2BGR)
        #img = cv2.cvtColor(orig, cv2.COLOR_BGR2RGB)
        img = transforms.ToTensor()(orig)
        img = img.unsqueeze(0)
        #img = transform_fn(img)

        #img = torch.from_numpy(img).to(device)
        #img = img.half() if half else img.float()  # uint8 to fp16/32
        #img = img / 255.0  # 0 - 255 to 0.0 - 1.0
        #if len(img.shape) == 3:
        #    img = img[None]  # expand for batch dim
        #print(img.shape)
        #print(img)
        out = model(img, augment=True, kp_flip=data['kp_flip'], scales=data['scales'], flips=data['flips'])[0]
        person_dets, kp_dets = run_nms(data, out)


      

#        print(out)
#        cv2.imshow('image', img)
#        cv2.waitKey(1)
    except Exception as err:
        print (err)

def start_node():
    with open(args.data) as f:
        global data
        data = yaml.safe_load(f)  # load data dict

    # add inference settings to data dict
    data['imgsz'] = args.imgsz
    data['conf_thres'] = args.conf_thres
    data['iou_thres'] = args.iou_thres
    data['use_kp_dets'] = not args.no_kp_dets
    data['conf_thres_kp'] = args.conf_thres_kp
    data['iou_thres_kp'] = args.iou_thres_kp
    data['conf_thres_kp_person'] = args.conf_thres_kp_person
    data['overwrite_tol'] = args.overwrite_tol
    data['scales'] = args.scales
    data['flips'] = [None if f == -1 else f for f in args.flips]
    data['count_fused'] = False

    global device
    device = select_device(args.device, batch_size=1)
    print('Using device: {}'.format(device))

    global model
    model = attempt_load(args.weights, map_location=device)  # load FP32 model
    global half
    half = args.half & (device.type != 'cpu')
    if half:  # half precision only supported on CUDA
        model.half()
    stride = int(model.stride.max())  # model stride

    imgsz = check_img_size(args.imgsz, s=stride)  # check image size
    dataset = LoadWebcam(pipe='0', img_size=imgsz, stride=stride)

    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once


    rospy.init_node('img_proc')
    rospy.loginfo('img_proc node started')
    rospy.Subscriber("usb_cam/image_raw", Image, process_image)
    rospy.spin()

def options():
    parser = argparse.ArgumentParser()
    # video options
    parser.add_argument('--color', type=int, nargs='+', default=[255, 255, 255], help='pose color')
    parser.add_argument('--face', action='store_true', help='plot face keypoints')
    parser.add_argument('--display', action='store_true', help='display inference results')
    parser.add_argument('--fps-size', type=int, default=1)
    parser.add_argument('--gif', action='store_true', help='create gif')
    parser.add_argument('--gif-size', type=int, nargs='+', default=[480, 270])
    parser.add_argument('--kp-size', type=int, default=2, help='keypoint circle size')
    parser.add_argument('--kp-thick', type=int, default=2, help='keypoint circle thickness')
    parser.add_argument('--line-thick', type=int, default=3, help='line thickness')
    parser.add_argument('--alpha', type=float, default=0.4, help='pose alpha')
    parser.add_argument('--kp-obj', action='store_true', help='plot keypoint objects only')
    parser.add_argument('--csv', action='store_true', help='write results so csv file')

    # model options
    parser.add_argument('--data', type=str, default='../kapao/data/coco-kp.yaml')
    parser.add_argument('--imgsz', type=int, default=1024)
    parser.add_argument('--weights', default='../kapao/kapao_s_coco.pt')
    parser.add_argument('--device', default='cpu', help='cuda device, i.e. 0 or cpu')
    parser.add_argument('--half', action='store_true')
    parser.add_argument('--conf-thres', type=float, default=0.5, help='confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='NMS IoU threshold')
    parser.add_argument('--no-kp-dets', action='store_true', help='do not use keypoint objects')
    parser.add_argument('--conf-thres-kp', type=float, default=0.5)
    parser.add_argument('--conf-thres-kp-person', type=float, default=0.2)
    parser.add_argument('--iou-thres-kp', type=float, default=0.45)
    parser.add_argument('--overwrite-tol', type=int, default=50)
    parser.add_argument('--scales', type=float, nargs='+', default=[1])
    parser.add_argument('--flips', type=int, nargs='+', default=[-1])

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    try:
        args = options()
        start_node()
    except rospy.ROSInterruptException:
        pass

